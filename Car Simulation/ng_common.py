# -*- coding: utf-8 -*-
"""
Common class for a genetic evolution simulation

Created on Fri Oct 09

@author: Erik
"""
import graphics

def SympyPt2DrawPoint(sympy_pt1, sympy_pt2):
	pt1 = graphics.Point(int(sympy_pt1.x), int(sympy_pt1.y))
	pt2 = graphics.Point(int(sympy_pt2.x), int(sympy_pt2.y))
	return graphics.Line(pt1, pt2)

def DrawCurrentState(win,*elements):
	for elem in elements:
		elem.Undraw(win)
		elem.Draw(win)
	win.redraw()