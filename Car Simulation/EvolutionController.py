# -*- coding: utf-8 -*-
"""
Class for performing evolution

Created on Wed Oct 14

@author: Erik
"""

import Agent as a
import Environment as e

class EvolutionController():

	# Class variables
	#death_p
	#c_p
	#m_p
	pop = []
	#pop_size

	def __init__(self, death_prob, mutate_prob, crossover_prob):
		self.death_p = death_prob; self.c_p = crossover_prob; self.m_p = mutate_prob;

	def SetEnvironment(self, environment):
		self.env = environment

	def GeneratePopulation(self, population_size, agent):
		self.pop = []; self.pop_size = population_size;
		for i in range(0,population_size): # clone the input agent across the population (new weights are generated for each)
			temp = a.Agent(agent.geometry, agent.m_p, agent.c_p)
			self.pop.append(temp)

	def Generation(self):
		fitnesses = []
		for agt in self.pop:
			fitness = agt.EvaluateFitness(self.env, 24, 50)
			print(fitness)
			fitnesses.append(fitness)
		return fitnesses

