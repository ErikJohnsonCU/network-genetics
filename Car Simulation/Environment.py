# -*- coding: utf-8 -*-
"""
Environment class for a 2D maze environment

Created on Thu Oct 08

@author: Erik
"""

import graphics
import sympy
from sympy import Polygon,Point,Segment
from ng_common import SympyPt2DrawPoint

class Environment():
	
	# Class variables
	geometry = []
	onscreen = []
	
	def __init__(self, *polygons):
		if (len(polygons) > 0):
			self.geometry = polygons
	
	def DefineGeometry(self, *polygons): # define the geometry for the environment
		self.geometry = polygons

	def AppendGeometry(self, *polygons): # add the polygons to the geometry
		for poly in polygons:
			self.geometry.append(poly)

	def Draw(self, win): # draw the environment using the graphics package
		lines = []
		for poly in self.geometry:
			lines.append(SympyPt2DrawPoint(poly.vertices[0], poly.vertices[len(poly.vertices)-1]))
			for i in range(0,len(poly.vertices)-1):
				lines.append(SympyPt2DrawPoint(poly.vertices[i], poly.vertices[i+1]))
		for line in lines:
			self.onscreen.append(line)
			line.draw(win)

	def Undraw(self, win): # removes the last drawn image of this environment
		for un in self.onscreen:
			un.undraw()
		
	def IsAgentValid(self, agent): # check if a point is valid from agent
		# build lists of the lines in both the environment and the agent
		lines_env = []
		lines_agt = []
		lines_agt = agent.GetLines()
		lines_env = self.GetLines()
		# test for interesection between any line in the agent and environment lists
		for line_agt in lines_agt:
			for line_env in lines_env:
				if len(line_agt.intersection(line_env)) > 0:
					return False
		return True # if no intersection exists

	def IsPolyValid(self, polygon): # check if a point is valid from polygon
		# build lists of the lines in both the environment and the agent
		lines_env = []
		lines_agt = []
		lines_agt.append(Segment(polygon.vertices[0], polygon.vertices[len(polygon.vertices)-1]))
		for i in range(0,len(polygon.vertices)-1):
			lines_agt.append(Segment(polygon.vertices[i], polygon.vertices[i+1]))
		for poly in self.geometry:
			lines_env.append(Segment(poly.vertices[0], poly.vertices[len(poly.vertices)-1]))
			for i in range(0,len(poly.vertices)-1):
				lines_env.append(Segment(poly.vertices[i],poly.vertices[i+1]))
		# test for interesection between any line in the agent and environment lists
		for line_agt in lines_agt:
			for line_env in lines_env:
				if len(line_agt.intersection(line_env)) > 0:
					return False
		return True # if no intersection exists

	def GetLines(self): # returns the line segments that make up the environment
		lines_env = []
		for poly in self.geometry:
			lines_env.append(Segment(poly.vertices[0], poly.vertices[len(poly.vertices)-1]))
			for i in range(0,len(poly.vertices)-1):
				lines_env.append(Segment(poly.vertices[i],poly.vertices[i+1]))
		return lines_env
		
if __name__ == '__main__': # debugging space
	pass