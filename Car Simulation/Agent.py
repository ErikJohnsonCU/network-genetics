# -*- coding: utf-8 -*-
"""
Agent class for a 2D maze searcher

Created on Thu Oct 08

@author: Erik
"""

import graphics
import sympy
from sympy import Polygon,Point,Segment
from ng_common import SympyPt2DrawPoint
import pybrain
from pybrain.tools.shortcuts import buildNetwork
from pybrain.structure import FeedForwardNetwork
from pybrain.structure import LinearLayer, SigmoidLayer
from pybrain.structure import FullConnection
import numpy as np
import random as rnd
import csv

class Agent():

	# Class variables
	#geometry
	#brain
	#input_size
	onscreen = []

	def __init__(self, polygon):
		# set the geometry of the agent
		self.geometry = polygon
		# creation of the mind
		self.brain = FeedForwardNetwork(); self.input_size = 4;
		inLayer = LinearLayer(4); hiddenLayer = SigmoidLayer(5); outLayer = SigmoidLayer(3)
		self.brain.addInputModule(inLayer); self.brain.addModule(hiddenLayer); self.brain.addOutputModule(outLayer)
		in_to_hidden = FullConnection(inLayer, hiddenLayer); hidden_to_out = FullConnection(hiddenLayer, outLayer)
		self.brain.addConnection(in_to_hidden); self.brain.addConnection(hidden_to_out); self.brain.sortModules();

	def ModifyBrain(self, input_N, hidden_N, output_N): # changes the structure of the brain to fit different problems
		# creation of the mind
		self.brain = FeedForwardNetwork(); self.input_size = input_N;
		inLayer = LinearLayer(input_N); hiddenLayer = SigmoidLayer(hidden_N); outLayer = SigmoidLayer(output_N)
		self.brain.addInputModule(inLayer); self.brain.addModule(hiddenLayer); self.brain.addOutputModule(outLayer)
		in_to_hidden = FullConnection(inLayer, hiddenLayer); hidden_to_out = FullConnection(hiddenLayer, outLayer)
		self.brain.addConnection(in_to_hidden); self.brain.addConnection(hidden_to_out); self.brain.sortModules()

	def Think(self, activations): # produce thinking
		if (len(activations) != self.input_size): # check against network input layer size
			return -100 # key error value
		thoughts = self.brain.activate(np.asarray(activations)) # activate the network (converting input to numpy)
		return thoughts.tolist() # return list of output from network

	def DefineGeometry(self, polygon): # define the geometry for the agent
		self.geometry = polygon

	def Move(self, translate_x, translate_y, rotate_rad): # move and rotate the agent
		origin_mv = Point(self.geometry.centroid.x, self.geometry.centroid.y)
		if (rotate_rad != 0):
			self.geometry = self.geometry.translate(-1*origin_mv.x, -1*origin_mv.y)
			self.geometry = self.geometry.rotate(rotate_rad)
			#self.geometry = self.geometry.translate(origin_mv.x-10,origin_mv.y-10)
			self.geometry = self.geometry.translate(origin_mv.x + translate_x, origin_mv.y + translate_y)
		else:
			self.geometry = self.geometry.translate(translate_x, translate_y)
		#print('x: ' + str(translate_x))
		#print('y: ' + str(translate_y))

	def EvaluateFitness(self, environment, accel, max_steps):
		# environment doesn't move, so only get lines once
		lines_env = environment.GetLines()
		# get start point
		start = self.geometry.centroid
		for i in range(0, max_steps):
			# compute activation
			verts_agt = self.GetVertices()
			min_dists = []
			for vertex in verts_agt:
				temp = []
				for line in lines_env:
					temp.append(line.distance(vertex))
				min_dists.append(min(temp))
			output = self.Think(min_dists)
			# act upon the output of the activation
			self.Move(output[0]*accel,output[1]*accel,0)
			if (not environment.IsAgentValid(self)): # check for collision
				break
		# evaluate the fitness as distance from start to end
		final = self.geometry.centroid
		return round(final.distance(start))

	def Mutate(self, m_p):
		weights = self.brain.params
		list_weights = weights.tolist()

		new_list_weights = []
		for elem in list_weights:
			if (rnd.random() < m_p):
				new_list_weights.append(rnd.uniform(-1*max(list_weights),max(list_weights)))
			else:
				new_list_weights.append(elem)

		weights = np.asarray(new_list_weights)

		self.brain._setParameters(weights)
		self.brain.sortModules()
		self.brain.reset()

	def Crossover(self, other, c_p):
		if (rnd.random() > c_p): # not crossing over
			return

		weights_this = self.brain.params
		list_weights_this = weights_this.tolist()
		weights_other = other.brain.params
		list_weights_other = weights_other.tolist()
		
		crossover_pt = rnd.randint(1, len(weights_this)-1) # generate the crossover pt

		list_weights_this = list_weights_this[:crossover_pt]
		list_weights_this.extend(list_weights_other[crossover_pt:]) # join the lists

		weights_this = np.asarray(list_weights_this)

		self.brain._setParameters(weights_this)
		self.brain.sortModules()
		self.brain.reset()

	def Draw(self, win): # draw the agent using the graphics package
		lines = []
		lines.append(SympyPt2DrawPoint(self.geometry.vertices[0], self.geometry.vertices[len(self.geometry.vertices)-1]))
		for i in range(0,len(self.geometry.vertices)-1):
			lines.append(SympyPt2DrawPoint(self.geometry.vertices[i],self.geometry.vertices[i+1]))
		for line in lines:
			self.onscreen.append(line)
			line.draw(win)

	def Undraw(self, win): # removes the last drawn image of this environment
		for un in self.onscreen:
			un.undraw()

	def GetLines(self): # returns the line segments that make up the agent
		lines_agt = []
		lines_agt.append(Segment(self.geometry.vertices[0], self.geometry.vertices[len(self.geometry.vertices)-1]))
		for i in range(0,len(self.geometry.vertices)-1):
			lines_agt.append(Segment(self.geometry.vertices[i],self.geometry.vertices[i+1]))
		return lines_agt

	def GetVertices(self): # returns the vertices that make up the agent
		return self.geometry.vertices

	def SaveAgent(self, filename): # saves the agent to a csv file
		with open(filename, 'wb') as csvfile:
			wr = csv.writer(csvfile, delimiter=',')
			weights = self.brain.params
			list_weights = weights.tolist()
			wr.writerow(list_weights)

	def LoadAgent(self, filename): # overwrites the agents brain parameters with those loaded from the csv file
		with open(filename, 'rb') as csvfile:
			rd = csv.reader(csvfile, delimiter=',')
			for row in rd:
				if len(row) > self.input_size:
					print(self.brain.params)
					list_weights = map(float, row)
					weights = np.asarray(list_weights)
					self.brain._setParameters(weights)
					self.brain.sortModules()
					self.brain.reset()
					print(self.brain.params)


if __name__ == '__main__': # debugging space
	agt = Agent(Polygon(Point(40,500),Point(40,525),Point(65,525),Point(65,500)))
	print(agt.brain.params)
	raw_input("Original values")
	agt.Mutate()
	print(agt.brain.params)
	raw_input("Mutated values")
	pass