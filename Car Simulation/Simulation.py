# -*- coding: utf-8 -*-
"""
Simulation sandbox for a genetic evolution simulation

Created on Sat Oct 10

@author: Erik
"""
import graphics
from sympy import Polygon,Point,Segment
import Environment as e
import Agent as a
from ng_common import DrawCurrentState
import time
import VisualController as vc
import EvolutionController as c
import random as rnd
import os

#env = e.Environment(Polygon(Point(20,20),Point(20,550),Point(750,550),Point(750,20)),Polygon(Point(100,550),Point(500,550),Point(500,100),Point(100,100)))
#agt = a.Agent(Polygon(Point(40,500),Point(40,525),Point(65,525),Point(65,500)))
#ctr = c.EvolutionController(0.2, 0.2, 0.05)
#ctr.SetEnvironment(env)
#ctr.GeneratePopulation(100, agt)
#for i in range(0,29):
#	print('')
#	print("Generation " + str(i+1))
#	print("================================")
#
#	fitnesses = ctr.Generation() # fitness evaluation (will soon have everything.. including evolution)
#	new_pop = [agent for fit,agent in sorted(zip(fitnesses,ctr.pop), reverse=True)] # order by best to worst
#
#	# save 10 best as csv files in their own directory
#	if not os.path.exists(os.path.dirname('results/')):
#		os.mkdir(os.path.dirname('results/'))
#	if not os.path.exists(os.path.dirname('results/gen_' + str(i) + '/')):
#		os.mkdir(os.path.dirname('results/gen_' + str(i) + '/'))
#	for j in range(0,10):
#		new_pop[j].SaveAgent('results/gen_' + str(i) + '/best_agent_' + str(rnd.randint(0, 10000)) + '.csv')
#	ctr.pop = new_pop[0:int(len(ctr.pop)/1.2)] # kill some
#	ctr.pop_size = len(ctr.pop)
#
#	# evolve (needs to move to evolution controller at some point)
#	for agent in ctr.pop:
#		agent.Mutate(ctr.m_p)
#		agent.Crossover(ctr.pop[rnd.randint(0, len(ctr.pop)/2)], ctr.c_p)
#
#raw_input("Press ENTER to exit")

# a quick visual simulation loaded from file
env = e.Environment(Polygon(Point(20,20),Point(20,550),Point(750,550),Point(750,20)),Polygon(Point(100,550),Point(500,550),Point(500,100),Point(100,100)))
agt = a.Agent(Polygon(Point(40,500),Point(40,525),Point(65,525),Point(65,500)))
agt.LoadAgent('results/gen_1/best_agent_358.csv') # load from file here
ctr = vc.VisualController(agt,env,800,600,24)
ctr.Simulate(.1)
raw_input("Press ENTER to exit")

## the dumbest simulation ever
#agt = a.Agent(Polygon(Point(52,52),Point(52,62),Point(62,62),Point(62,52)))
#env = e.Environment(Polygon(Point(10,10),Point(10,150),Point(150,150),Point(150,10)))
#win = graphics.GraphWin("Simple simulation", 200, 200, autoflush=False)
#DrawCurrentState(win,agt,env)
#print(env.IsAgentValid(agt))
#raw_input("Press ENTER to continue")
#while env.IsAgentValid(agt):
#	agt.Move(5,0,0)
#	DrawCurrentState(win,agt,env)
#	time.sleep(.5)
#	print(env.IsAgentValid(agt))
#raw_input("Press ENTER to exit")

## functional agent moving over time
#import time
#agt = a.Agent(Polygon(Point(20,20),Point(20,30),Point(30,30),Point(30,20)))
#win = graphics.GraphWin("Sample moving agent", 200, 200, autoflush=False)
#agt.Draw(win)
#raw_input("Press ENTER to continue")
#for i in range(0,10):
#	time.sleep(.5)
#	agt.Move(10,0,0)
#	agt.Undraw(win)
#	agt.Draw(win)
#	win.redraw()
#raw_input("Press ENTER to exit")

## testing the excitation and parameter extraction of the neural network
#agt = a.Agent(Polygon(Point(20,20),Point(20,30),Point(30,30),Point(30,20)))
#print(agt.brain.activate([1,2,3,4,5,6,7,8,9,10,11,12]))
#for mod in agt.brain.modules:
#	print("Module:", mod.name)
#	if mod.paramdim > 0:
#		print("--parameters:", mod.params)
#	for conn in agt.brain.connections[mod]:
#		print("-connection to", conn.outmod.name)
#		if conn.paramdim > 0:
#			print("- parameters", conn.params) # this gives the weights of the current connection

## testing the move function of agent
#agt = a.Agent(Polygon(Point(20,20),Point(20,30),Point(30,30),Point(30,20)))
#win = graphics.GraphWin("Sim",200,200)
#agt.Draw(win)
#raw_input("Press ENTER to continue")
#agt.Move(20,-10,1.4)
#agt.Draw(win)
#raw_input("Press ENTER to exit")

## testing IsPolyValid method
#env = e.Environment(Polygon(Point(10,10),Point(10,50),Point(50,50),Point(50,10)),Polygon(Point(15,15),Point(25,25),Point(35,25),Point(35,15)))
#agt = a.Agent(Polygon(Point(20,20),Point(20,30),Point(30,30),Point(30,20)))
#win = graphics.GraphWin("Sim",200,200)
#env.Draw(win)
#agt.Draw(win)
#print(env.IsPolyValid(agt.geometry))
#raw_input("Press ENTER to exit")

## testing agent drawing
#a = Agent()
#a.DefineGeometry(Polygon(Point(20,20),Point(20,30),Point(30,30),Point(30,20)))
#win = graphics.GraphWin("Sample agent", 200, 200)
#a.Draw(win)
#raw_input("Press ENTER to exit")

## testing environment drawing
#e = Environment()
#e.DefineGeometry(Polygon(Point(10,10),Point(10,50),Point(50,50),Point(50,10)))
#win = graphics.GraphWin("Sample environment", 200, 200)
#e.Draw(win)
#raw_input("Press ENTER to exit")