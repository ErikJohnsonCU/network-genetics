# -*- coding: utf-8 -*-
"""
Agent class for a 2D maze searcher

Created on Mon Oct 12

@author: Erik
"""

import Agent
import Environment
import graphics as g
from ng_common import DrawCurrentState
import time
from sympy import Polygon,Point,Segment

class VisualController():

	# Class variables
	#agt
	#env
	#win
	#accel

	def __init__(self, agent, environment, size_x, size_y, acceleration):
		self.agt = agent; self.agt.ModifyBrain(4, 5, 3); self.env = environment;
		self.win = g.GraphWin("Simulation", size_x, size_y)
		self.accel = acceleration # must be less than size of agent or we could jump a boundary
		DrawCurrentState(self.win,self.agt,self.env)

	def TimeStep(self): # performs a single timestep in simulation time
		# compute activation for the agents brain
		# for this test, we will use the distance from each corner of the agent to the environment as the input
		verts_agt = self.agt.GetVertices() # may get weird behaviour if the order of vertices changes when rotating...
		lines_env = self.env.GetLines()
		min_dists = [] # this would cause the order in min_dists to change...
		for vertex in verts_agt:
			temp = []
			for line in lines_env:
				temp.append(line.distance(vertex))
			min_dists.append(min(temp))
		output = self.agt.Think(min_dists) # which would change the order of the inputs to the network
		print('Distances')
		for dist in min_dists:
			print(round(dist))
		print('Output')
		for i in range(0,len(output)):
			output[i] = output[i] - .5 # allow negative numbers
			print(output[i])
		# act upon the output of the activation
		self.agt.Move(output[0]*self.accel,output[1]*self.accel,0)
		# inform the user of the action
		DrawCurrentState(self.win,self.agt,self.env)

	def Simulate(self, timeconstant_s):
		while self.env.IsAgentValid(self.agt):
			self.TimeStep()
			time.sleep(timeconstant_s)
			print(self.env.IsAgentValid(self.agt))

if __name__ == '__main__':
	pass