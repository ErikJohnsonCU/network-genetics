# README #

This README is for the network-genetics repo originally created by E.C.M.Johnson.

### What is this repository for? ###

* Learning about Neural Networks and Genetic Optimization
* A log for the new things I try out in this area..

### Dependencies ###

* Sympy (specifically the geometry module)

### Contribution guidelines ###

* Bang face on keyboard
* Upload result

### Who do I talk to? ###

* Erik Johnson, erikjohnson@cmail.carleton.ca