import matplotlib.pyplot as plt
import numpy as np

from pylab import*
from scipy.io import wavfile

sampFreq, data = wavfile.read("test2.wav")
#data = data[0:500]

fourier = np.fft.fft(data)
fourier = fourier[0:len(fourier)/2]

w = np.fft.fft(data)
w = map(np.linalg.norm, w)
freqs = np.fft.fftfreq(len(data), d=float(1/float(sampFreq)))

#for coef,freq in zip(w,freqs):
#    if coef:
#        print('{c:>6} * exp(2 pi i t * {f})'.format(c=coef,f=freq))

save = zip(w,freqs)
w = []; freqs = [];
for coef,freq in save:
	if freq < 4000 and freq > 0:
		w.append(coef); freqs.append(freq);

maxPeak = sum(freqs)/len(freqs)
w = w/maxPeak

plt.plot(freqs, w)
plt.show()